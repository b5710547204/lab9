import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Main for run application.
 * @author Patinya Yongyai
 * @version 26.03.2015
 *
 */
public class Main {
	/**
	 * 
	 * @param args not used
	 * @throws FileNotFoundException if file not found
	 * @throws IOException if found error from input or output
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException{
		final String DICT_DIR = "D:/dictionary.txt";
		WordCounter word1 = new WordCounter();
		StopWatch stopWatch = new StopWatch();
		InputStream input = new FileInputStream(DICT_DIR);
		stopWatch.start();
		System.out.println("Counted syllables : " + word1.countWords(new FileInputStream(DICT_DIR))  + " ");
		stopWatch.stop();
		System.out.println("Counted Word : "+word1.getWord());
		System.out.println("Elapsed Time : "+stopWatch.getElapsed());
	}

}
