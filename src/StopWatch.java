public class StopWatch {
	/**Start and stop time.*/
	private double start,stop;
	/**For keep true or false.*/
	private boolean check;
	/**For convert nano time to second time.*/
	private final double nanoTime  = 1.0E+9;
	
	/**
	 * Get the difference of time between start and stop.
	 * @return start-stop if that time is running,return stop-start if that time isn't running
	 */
	public double getElapsed(){
		if(check==true)
			return start-stop;
		else
			return stop-start;
	}
	
	/**
	 * Check that time is running or not.
	 * @return true if that time is running,false if that time isn't running
	 */
	public boolean isRunning(){
		return check;
	}
	
	/**
	 * This class start count a time.
	 * If called this method,check will be true 
	 */
	public void start(){
		this.start = System.nanoTime() / nanoTime; 
		check = true;
	}
	
	/**
	 * This class stop count a time.
	 * If called this method,check will be false
	 */
	public void stop(){
		this.stop = System.nanoTime() / nanoTime;
		check = false;
	}

}
