import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

/**
 * Count words and syllables from text file.
 * @author Patinya Yongyai
 * @version 26.03.2015
 *
 */
public class WordCounter {
	private int countWord = 0;
	
	/**
	 * 
	 * @param word for count syllables of that word
	 * @return syllables of that word
	 */
	 public int countSyllables(String word){
		 int syllables = 0;
		 State state = State.START;
		 String newWord = word.replaceAll("'", "");
		 char[] words = newWord.toCharArray();
		 for(char c : words){
			 switch(state) {
			 case START:
				 if(c=='e'||c=='E')
					 state = State.E_FIRST;
				 else if(isVowel(c) || c == 'y' || c == 'Y')
					 state = State.VOWEL;
				 else if(c == '-')
					 state = State.NONWORD;
				 else if(Character.isLetter(c))
					 state = State.CONSONANT;
				 else
					 state = State.NONWORD;
				 break;
			 case CONSONANT:
				 if (c=='e'||c=='E')
					 state = State.E_FIRST;
				 else if(isVowel(c) || c == 'y' || c == 'Y')
					 state = State.VOWEL;
				 else if(c == '-')
					 state = State.DASH;
				 else if(Character.isLetter(c))
					 state = State.CONSONANT;
				 else
					 state = State.NONWORD;
				 break;
				 
			 case VOWEL:
				 if(isVowel(c))
					 state = State.VOWEL;
				 else if (Character.isLetter(c)){
					 state = State.CONSONANT;
					 syllables++;
				 }
				 else if(c == '-'){
					 state = State.DASH;
					 syllables++;
				 }
				 else
					 state = State.NONWORD;
				 break;
				 
			 case E_FIRST:
				 if (isVowel(c))
					 state = State.VOWEL;
				 else if(Character.isLetter(c)){
					 state = State.CONSONANT;
					 syllables++;
				 }
				 else if(c == '-'){
					 state = State.DASH;
				 }
				 else
					 state = State.NONWORD;
				 break;
				 
			 case DASH:
				 if (c=='e'||c=='E')
					 state = State.E_FIRST;
				 else if (isVowel(c) || c == 'y' || c == 'Y')
					 state = State.VOWEL;
				 else if(Character.isLetter(c))
					 state = State.CONSONANT;
				 else
					 state = State.NONWORD;
				 break;
				 
			 case NONWORD:
				 break;
				 
			 }
			 
		 }
		 if(state == State.VOWEL || (state == State.E_FIRST && syllables == 0))
			 syllables++;
		 else if(state == State.DASH || state == State.NONWORD)
			 syllables = 0;
		 return syllables;
	 }

	 /**
	  * Check character.
	  * @param c is character for in check in vowelList
	  * @return true if c is character in vowelList
	  */
	private boolean isVowel(char c) {
		String vowelList = "AEIOUaeiou";
		return vowelList.indexOf(c) >=0;
	}
	
	/**
	 * Count word.
	 * @param url is directory of file
	 * @return	count word
	 * @throws IOException if found error from input or output
	 */
	public int countWords(URL url) throws IOException{
			InputStream in = url.openStream();
			return countWords(in);
		

	}
	
	/**
	 * Count word.
	 * @param in is input stream
	 * @return word from that text file
	 * @throws IOException if found error from input or output
	 */
	public int countWords(InputStream in) throws IOException{
		BufferedReader buf = new BufferedReader(new InputStreamReader(in));
		int count = 0;
		PrintWriter write = new PrintWriter("D:/a.txt");
		while(buf.ready()){
			String word = buf.readLine();
			count += countSyllables(word);
			countWord++;
		}
		write.close();
		return count;
	}
	
	/**
	 * 
	 * @return word from that text file
	 */
	public int getWord(){
		return countWord;
	}

}
