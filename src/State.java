
/**
 * State of WordCounter.
 * @author Patinya Yongyai
 * @version 26.03.2015
 *
 */
public enum State {
	START,
	CONSONANT,
	E_FIRST,
	NONWORD,
	DASH,
	VOWEL
}
